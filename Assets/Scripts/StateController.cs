﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StateController : MonoBehaviour
{
    public VideoPlayer backgroundVideo;
    public Text storyText;
    public Text storyText1;
    public Text actionText;
    public Text actionText1;
    public Text actionText2;

    enum StoryState { Introduction, Letter1, Andover, Inspector, MedExaminer, Observation1, Overton, CrimePlace1, Observation2, Home1, Home2, EmptyRoom, Room, Street, Shop, Observation3, Nhouse, Inspector1, Mister1, Mister2, Train, Final1, 
    	              Letter2, Inspector3, Administrator, SecondWaitress, Parents, GirlRoom, Sister, Donald, Conclusion, letter3, railway_station, in_train, brother, route, the_cape, the_home, article, secretary, hospital, Lady_Clark, return_town, 
                      letter4, secretary1, game_over1, game_over2, thoughts, murder4, words, parents1, crime_scene_final, win_final}
    StoryState state;

    // Start is called before the first frame update
    void Start()
    {
        state = StoryState.Introduction;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case StoryState.Introduction: Introduction(); break;
            case StoryState.Letter1: Letter1(); break;
            case StoryState.Andover: Andover(); break;
            case StoryState.Inspector: Inspector(); break;
            case StoryState.MedExaminer: MedExaminer(); break;
            case StoryState.Observation1: Observation1(); break;
            case StoryState.Overton: Overton(); break;
            case StoryState.CrimePlace1: CrimePlace1(); break;
            case StoryState.Observation2: Observation2(); break;
            case StoryState.Home1:Home1();break;
            case StoryState.Home2:Home2();break;
            case StoryState.EmptyRoom:EmptyRoom();break;
            case StoryState.Room:Room();break;
            case StoryState.Street:Street();break;
            case StoryState.Shop:Shop();break;
            case StoryState.Observation3:Observation3();break;
            case StoryState.Nhouse:Nhouse();break;
            case StoryState.Inspector1:Inspector1();break;
            case StoryState.Mister1:Mister1();break;
            case StoryState.Mister2:Mister2();break;
            case StoryState.Train:Train();break;
            case StoryState.Final1:Final1();break;
            case StoryState.Letter2:Letter2();break;
            case StoryState.Inspector3:Inspector3();break;
            case StoryState.Administrator:Administrator();break;
            case StoryState.SecondWaitress:SecondWaitress();break;
            case StoryState.Parents:Parents();break;
            case StoryState.GirlRoom:GirlRoom();break;
            case StoryState.Sister:Sister();break;
            case StoryState.Donald:Donald();break;
            case StoryState.Conclusion:Conclusion();break;
            case StoryState.letter3:letter3();break;
            case StoryState.railway_station:railway_station();break;
            case StoryState.in_train:in_train();break;
            case StoryState.brother:brother();break;
            case StoryState.route:route();break;
            case StoryState.the_cape:the_cape();break;
            case StoryState.the_home:the_home();break;
            case StoryState.article:article();break;
            case StoryState.secretary:secretary();break;
            case StoryState.hospital:hospital();break;
            case StoryState.Lady_Clark:Lady_Clark();break;
            case StoryState.return_town:return_town();break;
            case StoryState.letter4:letter4();break;
            case StoryState.secretary1:secretary1();break;
            case StoryState.game_over1:game_over1();break;
            case StoryState.game_over2:game_over2();break;
            case StoryState.thoughts:thoughts();break;
            case StoryState.murder4:murder4();break;
            case StoryState.words:words();break;    
            case StoryState.parents1:parents1();break;  
            case StoryState.crime_scene_final:crime_scene_final();break;
            case StoryState.win_final:win_final();break;        


        }
    }

    void SetTextPosition(Text element, float x, float y)
    {
        var rect = element.GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector3(x, y, 0);
    }

    void SetTextSize(Text element, float width, float height)
    {
        var rect = element.GetComponent<RectTransform>();
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }


    void Introduction()
    {

        backgroundVideo.source = VideoSource.VideoClip;
        backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/1");

        storyText.text = @"- In June 1935, I was back in England. I felt like I had one unfinished business left here. Today I will tell you about one of my most mysterious investigations, but unfortunately, I have some memory leaks in my head, so I hope the story which you will rewrite today will have, ironically speaking, a happy ending.";
        SetTextPosition(storyText, -97.3f, 50f);
        SetTextSize(storyText, 530f, 80f);
        storyText1.text = "";
        actionText1.text = "";
        actionText2.text = "";


        actionText.text = "Press Space to continue";
        SetTextPosition(actionText, -97.3f, -186f);
        SetTextSize(actionText, 530f, 30f);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            state = StoryState.Letter1;
        }
    }

    void Letter1()
    {
        backgroundVideo.source = VideoSource.VideoClip;
        backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/2");

         storyText.text = @"- Let’s begin with the fact that I received a very strange letter on previous days.";
         SetTextPosition(storyText, 200.3f, 10f);
         SetTextSize(storyText, 190f, 90f);

         storyText1.text =  @"Mr. Detective, perhaps you think that you are successfully solving these riddles, which turn out to be too difficult for our not-too-smart British policemen? Let's see how smart you really are. Maybe this nut will be too hard for you. Pay attention to Andover on the 21st of this month.
                                      
                                                                                                                     Sincerely yours, A.B.C";
         SetTextPosition(storyText1, -115.3f, 20f);
         SetTextSize(storyText1,  300f, 140f);

         actionText1.text = "";
         actionText2.text = "";

         actionText.text = "Press Space to go to Andover";
         SetTextPosition(actionText, -97.3f, -186f);
         SetTextSize(actionText, 530f, 30f);

          if (Input.GetKeyDown(KeyCode.Space))
        {
            state = StoryState.Andover;
        }
    }

    void Andover()
    {
    	  backgroundVideo.source = VideoSource.VideoClip;
    	  backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/3");

    	  storyText.text = @"- We are in Andover, now it's your turn to decide:";
    	  SetTextPosition(storyText, 120.3f, -150f);
          SetTextSize(storyText, 530f, 80f);

          storyText1.text ="- There were no fingerprints on the letter and there was no evidence to point me to the likely author. My knowledge and experience told me then that there was danger in this letter. On the 21st of this month, an elderly woman named Asher who owned a tiny tobacco shop in Andover was found murdered...";
          SetTextPosition(storyText1, 100.3f, 1f);
          SetTextSize(storyText1, 530f, 100f);

    	    actionText.text = "Press A to speak with inspector";
          SetTextPosition(actionText, -110.3f, -205f);
          SetTextSize(actionText, 530f, 60f);

          actionText1.text = "Press B to speak with medical examiner";
          SetTextPosition(actionText1, 350.3f, -205f);
          SetTextSize(actionText1, 530f, 60f);
          actionText2.text = "";

            if (Input.GetKeyDown(KeyCode.A))
        {
            state = StoryState.Inspector;
        }else if (Input.GetKeyDown(KeyCode.B)){
            state = StoryState.MedExaminer;
        }

    }
    void Inspector(){
        
    	   backgroundVideo.source = VideoSource.VideoClip;
    	   backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/4");
           storyText.text = @"- And you think that her husband often threatened her? ";
           SetTextPosition(storyText, 200.3f, 30f);
           SetTextSize(storyText, 190f, 90f);

             storyText1.text = @"  -Exactly. He was completely unbearable and often threatened her. It is still too early to say that he committed this murder for sure. However, I would like to hear from him how he spent yesterday evening.  ";
             SetTextPosition(storyText1, -100.3f, -30f);
             SetTextSize(storyText1, 190f, 150f);

             actionText.text = @"Press Space to continue";
             SetTextPosition(actionText, -97.3f, -186f);
             SetTextSize(actionText, 530f, 30f);
     
         actionText1.text = @"";
         actionText2.text = "";


         if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Observation1;
         }
    }

     void MedExaminer(){
    	   backgroundVideo.source = VideoSource.VideoClip;
    	   backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/5");
           storyText.text = @"- The woman was killed by a strong blow to the head, possibly while she was trying to reach for a pack of cigarettes on the shelf behind the counter. Death occurred seven to nine hours before the discovery of the murder. The murder weapon wasn’t found. It is impossible to say exactly what it was. It could be deduced that the woman was standing with her back to the killer when she was hit.   ";
            SetTextPosition(storyText, 5.3f, -100f);
            SetTextSize(storyText, 530f, 80f);

           storyText1.text = @" ";
           actionText2.text = "";
           
           actionText.text = @"Press Space to continue";
            SetTextPosition(actionText, -97.3f, -186f);
            SetTextSize(actionText, 530f, 30f);
            if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Observation1;
         }

           actionText1.text = @"";
    }
     
     void Observation1(){
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/6");
         storyText.text = @" - We now have an argument in defense of the husband. If he insulted and threatened his wife, then she probably would have stood in front of him. Instead, she turns her back on her future assassin in an attempt to take him tobacco from the shelf.";
         SetTextPosition(storyText, 100.3f, -90f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"";
         actionText.text = @"Press A to go to the Overton and speak with woman's niece";
         actionText1.text = @"Press B to visit the place of crime ";
         actionText2.text = "";
          if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.Overton;
         }else if (Input.GetKeyDown(KeyCode.B)){
         	state = StoryState.CrimePlace1;
         }
    }

     void Overton(){
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/9");
         storyText.text = @" - Mary, tell me please, was he threatening her, wasn’t he? ";
         SetTextPosition(storyText, 160.3f, 20f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"- Of course, he told her terrible things. My aunt said that he was a very nice young man when she married him. It didn't even occur to me for a second that he could do it. It always seemed to me that all these were simple threats and there was nothing more…  ";
         SetTextPosition(storyText1, 10.3f, -100f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press A to go to Andover";
         actionText1.text = @"Press B to go back ";
         actionText2.text = "";
         if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.CrimePlace1;
         }else  if (Input.GetKeyDown(KeyCode.B)){
             state = StoryState.Andover;
         }
         
          
         
    }
    void CrimePlace1(){
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/13");
         storyText.text = @"- I looked around. Doubtful establishment. A railway directory was found in the store opened on the Andover page. The killer is probably not local. No fingerprints were found either.  ";
         SetTextPosition(storyText, 100.3f, -80f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"";
         actionText.text = @"Press Space to leave the shop";
         actionText1.text = @"";
         actionText2.text = "";
         

         if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Observation2;
    }

}

 void Observation2(){
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/14");
         storyText.text = @"- Unfortunately, the inspection of the crime scene did not give us many answers. I talked to the local people and found out that Mrs. Asher lived at the store. I think we’ll pay her house a visit.  ";
         SetTextPosition(storyText, 100.3f, -50f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"";
         actionText.text = @"Press Space to continue";
         actionText1.text = @"";
         actionText2.text = "";

         if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Home1;
    }

}

 void Home1(){
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/15");
         storyText.text = @" - There was a rather large door behind the store, I entered it and got straight into the house. ";
         SetTextPosition(storyText, 80.3f, -20f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @" - Hmm, everything is very clean everywhere,  but it looks bleak and the environment is very sparse. On the shelf in the hallway there were some kind of strange photographs, by the way I did not find the woman's husband there ...";
         SetTextPosition(storyText1, 60.3f, -90f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press Space go further";
         actionText1.text = @"";
         actionText2.text = "";
         

         if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Home2;
    }

}

 void Home2(){
         
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/16");
         storyText.text = @" - A staircase from the hallway leads to two rooms. Unfortunately, I have time to examine only one of them. Which one should I examine? ";
         SetTextPosition(storyText, 100.3f, -70f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @" ";
         actionText2.text = "";
    

         actionText.text = @"Press A to inspect the room of the murdered";
         actionText1.text = @" Press B to inspect a suspicious empty room";
         

         if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.Room;
    }else if (Input.GetKeyDown(KeyCode.B)){
        state = StoryState.EmptyRoom;
    }

}
 void EmptyRoom(){
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/17");
         storyText.text = @" - After the inspection, I did not find anything interesting here. Just one raincoat and one sweater hanging from the doorknob and this dusty old bed. It's empty under it...";
         SetTextPosition(storyText, 100.3f, 20f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"";
         actionText.text = @"Press Space to continue";
         actionText1.text = @" ";
         actionText2.text = "";

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Street;
         

     }
 }

      void Room(){
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/18");
         storyText.text = @"- I searched. Nothing fishy, ​​a couple of old blankets on the bed, a stack of washed linen in one drawer, and recipes in the other. If there were any personal documents in the room, is highly likely that the police took them. ";
         SetTextPosition(storyText, 100.3f, -50f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"";
         actionText.text = @"press Space to continue";
         actionText1.text = @" ";
         actionText2.text = "";

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Street;
         }
     }

      void Street(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/19");
         storyText.text = @"- Almost opposite to Mrs. Asher's shop, there was a green shop, one of which most of the goods are were sold on the street. The house and the shop to Mrs. Asher's right were empty. On the windows there was an inscription on the white paper {for rent}. On the other side was the neighbor's house.";
         SetTextPosition(storyText, 90.3f, -80f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"";
         actionText.text = @"Press A to investigate the shop";
         actionText1.text = @"Press B to investigate the neighbor’s house";
         actionText2.text = "";
         
          if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.Shop;
         }else if (Input.GetKeyDown(KeyCode.B)){
            state = StoryState.Nhouse;
         }

    }

     void Shop(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/20");
         storyText.text = @" - It happened across from you, right? I mean, this murder ... You may even have seen the killer walk into the store: a tall, blond man, right?";
         SetTextPosition(storyText, 90.3f, -50f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"- You know, I didn't notice so much last night - it's a fact. In the evenings we are usually busy - we always have quite a lot of customers coming home from work. A tall, fair-haired man with a beard ... No, I can't say that yesterday I saw someone who looked according to this description.";
         SetTextPosition(storyText1, 10.3f, -130f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press Space to continue";
         actionText1.text = @" ";
         actionText2.text = "";
           
           if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Observation3;
         }

    }

     void Observation3(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/21");
         storyText.text = @"- The evening time is considered busy in the Andover city, everyone is going about their business and a fairly large number of people are walking along the sidewalks. Our killer has chosen the moment very well …";
         SetTextPosition(storyText, 100.3f, -80f);
         SetTextSize(storyText, 530f, 100f);
         storyText1.text = @"";
         actionText.text = @"press Space to continue";
         actionText1.text = @" ";
         actionText2.text = "";

        if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Inspector1;
         }
         
}

  void Nhouse(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/22");
         storyText.text = @" - Mrs. Asher was not that sociable. Life was not easy for her - everyone knew that. To be honest, her husband should have been imprisoned many years ago. Mrs. Asher was not very afraid of him - she turned into a real fury when she was hurt.";
         SetTextPosition(storyText, -50.3f, -160f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"- Has Mrs. Asher ever received unsigned letters or letters with signature initials such as A.B.C?";
         SetTextPosition(storyText1, 120.3f, -30f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"- There was nothing like this, I have not seen ...";
         SetTextPosition(actionText, 120.3f, -50f);
        
         actionText1.text = @"Press Space to continue";
         SetTextPosition(actionText1, -110.3f, -205f);
         actionText2.text = "";


           if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Inspector1;
         }

         
}

  void Inspector1(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/23");
         storyText.text = @" - Did any of the witnesses say that they saw Asher, the husband of the murdered woman?";
         SetTextPosition(storyText, 130.3f, -20f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"- Nobody. And this is another proof of his innocence. There are still two witnesses who need to be interrogated…";
         SetTextPosition(storyText1, 70.3f, -100f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press A to speak with Mr.Partidge (local postman)";
         SetTextPosition(actionText, -110.3f, -205f);
         SetTextSize(actionText, 530f, 100f);
         
         actionText1.text = @"Press B to speak with Mr.Albert (local store guard)";
         SetTextPosition(actionText1, 350.3f, -205f);
         SetTextSize(actionText1, 530f, 100f);
         
         actionText2.text =" ";
         SetTextPosition(actionText2, 550.3f, 550f);
          
          if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.Mister1;
         }else if (Input.GetKeyDown(KeyCode.B)){
            state = StoryState.Mister2;
         }
         
}

 void Mister1(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/24");
         storyText.text = @" - If I understand correctly, Mr. Partridge, you were the last person to see Mrs. Asher alive. Have you often made purchases in her store?";
         SetTextPosition(storyText, 100.3f, -20f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @" - Quite often since the store is located near my house. Unfortunately, I know nothing about Mrs. Asher except the name of the items I wanted to buy from her.";
         SetTextPosition(storyText1, 40.3f, -120f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Train;
         }
}

void Mister2(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/25");
         storyText.text = @"- Did you enter the store at 6 pm?";
         SetTextPosition(storyText, 260.3f, -30f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @" -  Exactly. I went inside, but there was no one there. I knocked on the counter and waited a little. Nobody showed up and I left ...";
         SetTextPosition(storyText1, 10.3f, -50f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press space to continue";
         
         actionText1.text = @"";
         actionText2.text = "";
          
          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Train;
         }
}


void Train(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/26");
         storyText.text = @"- We met with completely an unknown person. This person is hiding in the dark and obviously wants to stay there. On the one hand, we know nothing, but on the other hand, we already know quite a lot. I see his figure slowly emerging: this is a person who types well, buys expensive paper and have a powerful need of self expression.";
         SetTextPosition(storyText, 80.3f, -20f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"";
         SetTextPosition(storyText1, 70.3f, -50f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Final1;
         }
}

void Final1(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/27");
         storyText.text = @"- Unfortunately, we were unable to reveal anything. I'll take it as a failure at my own expense ...";
         SetTextPosition(storyText, 90.3f, -90f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"";
         SetTextPosition(storyText1, 70.3f, -50f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Letter2;
         }
}
void Letter2(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/28");
         storyText.text = @"        Dear Mr. Noting, 

        How do you feel today? I think I won the first round. 
        The fun is just beginning. Check out Backhill-on-Sea 
        on July 25 this year. 
        We live in very fascinating times.
                                         A.B.C ";
        
         SetTextPosition(storyText, -50f, -10f);
         SetTextSize(storyText, 530f, 150f);
         
         storyText1.text = @"- On July 25, the body of a young girl was found on the beach at Backhill. She was identified as Elizabeth Barnard. She worked as a waitress in a local cafe and she lived with her parents. The medical examiner determined that death occurred between half past 12 at night and one in the morning. Under the body was an alphabetical railway directory, opened to pages of trains to Backhill.";
         SetTextPosition(storyText1, 90.3f, -140f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Inspector3;
         }
         
}

void Inspector3(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/29");
         storyText.text = @"- I assume, you have no description of the murdered girl?";
         SetTextPosition(storyText, 120f, 20f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"- She was 23 years old and worked as a waitress in a local cafe. The girl was strangled by her own belt. So far, I have no other information ...";
         SetTextPosition(storyText1, 40.3f, -130f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to speak with the administrator cafe";
         actionText1.text = @"";
         actionText2.text = "";
          
          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Administrator;
         }
         
}

void Administrator(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/30");
         storyText.text = @"- What can you tell us about the murdered girl, Mister Marrion?";
         SetTextPosition(storyText, 110f, 50f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @" - She has been working here for the second summer. Elizabeth was a good waitress, welcoming and always in a good mood. She used to finish her working day at exactly 8 pm. Another waitress worked with her, you can talk to her, maybe she can tell you more ...";
         SetTextPosition(storyText1, 20.3f, -140f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press A to speak with the second waitress";
         actionText1.text = @"press B to speak with the girl's parents";
         actionText2.text = "";

         if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.SecondWaitress;
         }else if (Input.GetKeyDown(KeyCode.B)){
            state = StoryState.Parents;
         }
         
}

void SecondWaitress(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/31");
         storyText.text = @"- Did you know Elizabeth Barnard?";
         SetTextPosition(storyText, 200f, 20f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"- She has worked here longer than me. Betty was a very calm and friendly girl. I know she had a friend who worked for a real estate agency. She did not discuss her affairs with anyone in the cafe, but it was on that evening that she was supposed to meet with her friend.";
         SetTextPosition(storyText1, 20.3f, -140f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press Space to continue";
         actionText1.text = @"";
         actionText2.text = " ";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Conclusion;
         }
         
}

void Parents(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/32");
        
         storyText.text = @"-I know that this is very painful madam, but we need to know all the details, to start looking for the culprit as soon as possible. Is it right that you have 2 daughters? ";
         SetTextPosition(storyText, 90f, 50f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"-Exactly. The eldest daughter works in London. Betty was engaged to Donald Fraser, I respect him undeniably. She generally did not like saying where she was going or what she would do. She was a simple good girl with a decent boyfriend. Why would someone suddenly want to kill her? It doesn't make any sense ! ";
         SetTextPosition(storyText1, 60.3f, -140f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press A to inspect the girl’s room";
         actionText1.text = @"press B to speak with her sister";
         actionText2.text = "";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.GirlRoom;
         }else if (Input.GetKeyDown(KeyCode.B)){
            state = StoryState.Sister;
         }
         
}

void GirlRoom(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/33");
        
         storyText.text = @" Nothing suspicious… If there were any personal documents in the room, is highly likely that the police took them.";
         SetTextPosition(storyText, 10f, 50f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"";
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Conclusion;
         }
         
}


void Sister(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/34");
        
         storyText.text = @" - There was nothing wrong with Betty. She was always very straightforward. I was afraid that her boyfriend might leave her forever. And that would be awful. He is a very reliable and hardworking person. Donald takes everything very personally and he is very jealous. Once Don got so angry that Betty got very scared. If he loses his temper, he forgets about everything in the world.";
         SetTextPosition(storyText, 10f, 50f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"- Perhaps it is necessary to interrogate Donald.";
         
         SetTextPosition(storyText1, 200f, -110f);
         SetTextSize(storyText1, 530f, 100f);
         

         actionText.text = @"press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
         

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Donald;
         }
         
}

void Donald(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/35");
        
         storyText.text = @"- Did Elizabeth tell you where she decided to go tonight?";
         SetTextPosition(storyText, 110f, 50f);
         SetTextSize(storyText, 530f, 100f);
         
         storyText1.text = @"- I decided to trace her that evening when she left the cafe, as I was not sure of the veracity of the fact that she was supposed to see her friend. I went to St. Leonards and traced the approaching buses, but in the end I didn't find Betty and returned home.";
         SetTextPosition(storyText1, 110f, -100f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
       

         if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.Conclusion;
         }
         
}

void Conclusion(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/36");
        
         storyText.text = @"- There is one very important clue to solving a crime and that is its motive. The alphabetic complex undoubtedly takes place, but usually the madman very carefully justifies the crimes he commits. Victims are selected in alphabetical order because they present some kind of inconvenience to the perpetrator personally. There are no criminals who kill at random. The criminal either kills people who, in his opinion, stand in his way, or he commits his crimes out of conviction. The question arises, what is really going on in the mind of the killer? As you can see from his letters, he kills because of sporting interest. Could this be true? And even if this is true, then on what basis does he choose his victims, other than the order of letters in the alphabet?";
         SetTextPosition(storyText, 10f, 10f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"- Today I lost to him again, but now there is enough information to build a logical chain of his actions.";
         SetTextPosition(storyText1, 10f, -110f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to read the third letter ";
         actionText1.text = @"";
         actionText2.text = "";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);
         if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.letter3;
         }
}
void letter3(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/37");
        
         storyText.text = @"       Poor mister detective,

         It turns out that you are not that great at 
         solving small riddles. 
         Or are your best times over? 
         Let's see if you are more lucky this time.
         Kerston, 30th. 
         Try not to lose, or I'm already bored.
                                                              A.B.C";
         
         SetTextPosition(storyText, -40f, 30f);
         SetTextSize(storyText, 530f, 150f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, 10f, -110f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to go to the railway station ";
         SetTextPosition(actionText, 180.3f, -200f);
         SetTextSize(actionText, 130f, 100f);
         
         actionText1.text = @"";
         actionText2.text = "";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.railway_station;
         }
         
}

void railway_station(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/38");
        
         storyText.text = @"- No news yet. All people are involved. All people whose last name begins with C are warned by phone, if possible. Although there is little hope.";
         SetTextPosition(storyText, -20f, -30f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"- Now all the town will be looking for A.B.C, that's exactly what he wants. But this, perhaps, is his weakness. Being distracted by his accomplishments, he may lose caution at some point ... That's what I hope.";
         SetTextPosition(storyText1, 30f, -130f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to continue ";
         actionText1.text = @"";
         actionText2.text = "";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.in_train;
         }
         
}
void in_train(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/39");
        
         storyText.text = @"- Sir Carmichael Clarke was found with a broken head today. He was a very good otolaryngologist. Another interesting fact, he had a habit of walking every evening after dinner. A railway directory was also found next to the body.";
         SetTextPosition(storyText, 10f, 10f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @" - I feel that there are some inconsistencies in this alphabet too. If only I could understand ... then everything else would be much easier.";
         SetTextPosition(storyText1, 30f, -110f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to speak with Clarke’s brother ";
         SetTextPosition(actionText, 130.3f, -200f);
         SetTextSize(actionText, 270f, 100f);
         actionText1.text = @"";
         actionText2.text = "";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);
            if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.brother;
         }
}

void brother(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/40");
        
         storyText.text = @"- Did your brother, as I understand, behaved as usual yesterday? Did he receive unexpected emails? Did something upset him yesterday? ";
         SetTextPosition(storyText, 80f, 10f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @" - He behaved as usual. Excitement and frustration were normal for my brother, as his wife is very ill. He had a habit of going for a walk before bed, and he did it mandatory.";
         SetTextPosition(storyText1, -90f, -90f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"- I would like to see the route of his every night walk, if possible...";
         SetTextPosition(actionText, 80.3f, -150f);
         SetTextSize(actionText, 530f, 50f);
         
         actionText1.text = @"press Space to see the route";
         SetTextPosition(actionText1, 150.3f, -230f);
         SetTextSize(actionText1, 230f, 100f);
         actionText2.text = "";

          if (Input.GetKeyDown(KeyCode.Space)){
             state = StoryState.route;
         }
         
}

void route(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/41");
        
         storyText.text = @" - We moved down the road. At the end of the path, the path ran between thorny bushes of blackberries and ferns to the sea. Suddenly we came out onto a grassy promontory, from which an incredible view of the sea and the beach opened up. The location was awesome.";
         SetTextPosition(storyText, 130f, -200f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, 10f, -110f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"Press A to explore the cape";
         SetTextPosition(actionText, -170.3f, -250f);
         SetTextSize(actionText, 230f, 100f);
         actionText1.text = @"Press B to return home";
         SetTextPosition(actionText1, 200.3f, -250f);
         SetTextSize(actionText1, 230f, 100f);
         actionText2.text = "";
         

         if (Input.GetKeyDown(KeyCode.A)){
             state = StoryState.the_cape;
         } else if (Input.GetKeyDown(KeyCode.B)){
             state = StoryState.the_home;
         }
         
}

void the_cape(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/42");
        
         storyText.text = @"- This is where my brother used to walk in the evenings. He reached this point, then went up the path and turned right.I walked past the farm and across the field went straight to the house. I suppose the killers was hiding in the shadows.My brother didn’t notice anything until he received this blow.";
        
         SetTextPosition(storyText, 1f, -30f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, 10f, -110f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to return to the house";
         SetTextPosition(actionText, 10f, -200f);
         SetTextSize(actionText, 230f, 100f);
         
         actionText1.text = @"";
         actionText2.text = "";
          if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.the_home;
         }
         

          
}

void the_home(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/43");
        
         storyText.text = @"- After the assassination of Sir Carmichael Clarke, the riddle A.B.C was on the front pages of every newspaper. They just didn't write about anything else. All sorts of evidence that were allegedly found were constantly reported. Immediate and imminent arrests were reported.";
         SetTextPosition(storyText, 10f, -150f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, 10f, -110f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to continue";
         actionText1.text = @"";
         actionText2.text = "";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);
           if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.article;
         }

          }

 void article(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/44");
        
         storyText.text = @"KILLED SIR CARLMICLE CLARK HORRIBLE TRAGEDY IN KURSTON ACTS THE MANIAC KILLER";
         SetTextPosition(storyText, -100f, -60f);
         SetTextSize(storyText, 430f, 200f);
         
         storyText1.text =  @"Just a month ago, England was shocked by the murder of Elizabeth Bernard, 
a young girl, which took place in Backhill. Readers will probably remember that 
an alphabetical railway directory was found next to the victim's body. The same directory was found next to the body of Sir Clark
- so the police are inclined to believe that both murders were committed by
the same person. Is it possible that the killer is wandering around our seaside?";
         SetTextPosition(storyText1, -100f, -60f);
         SetTextSize(storyText1, 500f, 100f);

         actionText.text = @"press space to speak with Clark’s secretary";
         SetTextPosition(actionText, 20.3f, -200f);
         SetTextSize(actionText, 330f, 80f);
         actionText1.text = @"";
         actionText2.text = "";
         
           if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.secretary;
         }

          }

 void secretary(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/45");
        
         storyText.text = @"- Here's the thing, Mr. Detective. Mr. Clarke made it clear to you that I left Combside of my own accord. Actually, I was ready to stay in the house, but Lady Clarke wished me to leave. Of course, I understand that she is a very sick person and her brain is strongly influenced by the drugs she is taking. This makes her moody and suspicious. She developed an inexplicable hatred for me and immediately insisted on my living.";
         SetTextPosition(storyText, 10f, 10f);
         SetTextSize(storyText, 530f, 130f);
         
         storyText1.text =  @"- Thank you for daring to tell me about it.";
         SetTextPosition(storyText1, 200f, -110f);
         SetTextSize(storyText1, 530f, 130f);

         actionText.text = @"press A to go to the hospital to see Lady Clarke";
         SetTextPosition(actionText, -150.3f, -200f);
         SetTextSize(actionText, 430f, 100f);
         
         actionText1.text = @"press B to return to town";
         SetTextPosition(actionText1, 300.3f, -200f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = "";
    
           if (Input.GetKeyDown(KeyCode.A)){
              state = StoryState.hospital;
         } else if (Input.GetKeyDown(KeyCode.B)){
              state = StoryState.return_town;
         }

          }

          void hospital(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/46");
        
         storyText.text = @"- I think that her husband's death was a serious trauma for her? What did she think about her household staff before she was hospitalized?";
         SetTextPosition(storyText, 50f, -30f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"- You know, mister detective, not so strong. In her condition, Lady Clarke does not always adequately perceive what is happening. As far as I know, she treated all the household staff well, including her husband's secretary.";
         SetTextPosition(storyText1, 1f, -110f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to continue";
         actionText1.text = @"";
         actionText2.text = @"- May I pay a visit to Lady Clark?";
         SetTextPosition(actionText2, 180.3f, -200f);
         SetTextSize(actionText2, 530f, 100f);
           if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.Lady_Clark;
         }

          }
  void Lady_Clark(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/47");
        
         storyText.text = @"- Have they caught him already ?";
         SetTextPosition(storyText, 10f, -70f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"- No, they haven't caught it yet. There were a lot of strangers in the neighborhood. Holiday season. But vacationers usually go to the beaches. They rarely come to the house. No stranger approached the house that day ... as far as I know.";
         SetTextPosition(storyText1, 100f, -50f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"- This girl is a liar. I saw with my own eyes how she spoke to a completely unfamiliar man on the steps of the porch. It was in the morning, on the day of my husband's death.";
         SetTextPosition(actionText, -90.3f, -120f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"press space to continue";
         SetTextPosition(actionText1, 140.3f, -200f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);
           if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.letter4;
         }

          }
 void return_town(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/48");
        
         storyText.text = @"- Normally, these 3 tragedies would never be intertwined with each other. They would develop each in their own way and would not affect each other in any way. The complexity and confusion of what is happening in life never ceases to amaze me. I hope we won't lose the 4th murder to him ...";
         SetTextPosition(storyText, 10f, -70f);
         SetTextSize(storyText, 530f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, 100f, -50f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"";
         SetTextPosition(actionText, -90.3f, -120f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"press space to read the fourth letter";
         SetTextPosition(actionText1, 140.3f, -200f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);
           if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.letter4;
         }

          }

 void letter4(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/49");
        
         storyText.text = @"Still unsuccessful? Well, this is just indecent! What are you and the police doing? 
Well, where are you going to look for a clue now?
I'm just sorry for you.
But if something did not work out the first time, then you have to try again, and again, and again. 
We still have a long way to go. Our next little event will take place in Doncaster on September 11th.

Kind regards,  A.B.C";
         SetTextPosition(storyText, -110f, -20f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"- Perhaps, I'll start by re-interrogating this suspicious secretary...";
         SetTextPosition(storyText1, 200f, -240f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"";
         SetTextPosition(actionText, -90.3f, -120f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"press Space to continue";
         SetTextPosition(actionText1, -90.3f, -200f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, 140.3f, -100f);
         SetTextSize(actionText2, 530f, 100f);
           if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.secretary1;
         }
     }

 void secretary1(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/50");
        
         storyText.text = @"- Now I remember! I completely forgot about it! But it doesn't matter at all. He was just one of those people who go from house to house and sell stockings. I was just walking down the hall when he came to the door. Instead of calling, he spoke to me, but he looked completely harmless, which is probably why I forgot about him.";
         SetTextPosition(storyText, -110f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"- Stockings ... stockings ... stockings. Three months ago ... and later ... And now again ... Exactly! I got it !!!";
         SetTextPosition(storyText1, 120f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @" press A -  The killer is the secretary";
         SetTextPosition(actionText, -100.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @" press B - Check the crime scene one more time";
         SetTextPosition(actionText1, -100.3f, -160f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @" press C - Think about all the gathered facts and draw a logical chain";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);
           
           if (Input.GetKeyDown(KeyCode.A)){
              state = StoryState.game_over1;
         } else if (Input.GetKeyDown(KeyCode.B)){
             state = StoryState.game_over2;
         }else if (Input.GetKeyDown(KeyCode.C)){
             state = StoryState.thoughts;
         }
     

     }
        void game_over1(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/51");
        
         storyText.text = @" GAME OVER ";
         SetTextPosition(storyText, -110f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, 120f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"";
         SetTextPosition(actionText, -100.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"";
         SetTextPosition(actionText1, -100.3f, -160f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);
          
     }
   
     void game_over2(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/52");
        
         storyText.text = @" - We have already been here and found nothing ... This time we lost, it's time to return to my hometown .... ";
         SetTextPosition(storyText, -110f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @" GAME OVER ";
         SetTextPosition(storyText1, 120f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"";
         SetTextPosition(actionText, -100.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"";
         SetTextPosition(actionText1, -100.3f, -160f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);
          
     }

      void thoughts(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/53");
        
         storyText.text = @"- This can not be a coincidence. When Mr. Clark's secretary told about the stockings, I felt that this could lead us to something. And now I understand why, to the words of the neighbor of the first murdered Mrs. Asher. She talked about annoying people who are constantly trying to sell you something and mentioned stockings. Three crimes, and in front of each there is a man selling stockings and at the same time inspecting the area. ";
         SetTextPosition(storyText, -110f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, 120f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press Space to continue";
         SetTextPosition(actionText, -100.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"";
         SetTextPosition(actionText1, -100.3f, -160f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.murder4;
         }
          
     }

     void murder4(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/54");
        
         storyText.text = @"- Another alphabetical murder?  ";
         SetTextPosition(storyText, 180f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"- Yes, and this time everything is much more interesting. The killer used a knife. The railway directory lay on the floor near the body of the murdered man. This time A.B.C was wrong, the murdered man's name was George Erlsfield.";
         SetTextPosition(storyText1, -60f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"- I can guess who is the killer, but I need more information for the complete picture. ";
         SetTextPosition(actionText, 140.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"press space to continue";
         SetTextPosition(actionText1, -80.3f, -170f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.words;
         }
          
     }

     void  words(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/55");
        
         storyText.text = @"- Once a wise old Frenchman told me that a man invented a speech so that people would forget how to think. Conversation is the perfect way to find out what the other person wants to hide. A human being is unable to resist the temptation to open up in a conversation. And every time in an open conversation, a person will give himself away. ";
         SetTextPosition(storyText, 180f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, -60f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press A to go back and talk to Elizabeth's parents";
         SetTextPosition(actionText, 140.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"press B to visit the crime scene where the girl's body was found";
         SetTextPosition(actionText1, -80.3f, -170f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.A)){
              state = StoryState.parents1;
         } else if (Input.GetKeyDown(KeyCode.B)){
              state = StoryState.crime_scene_final;
         }
          
     }

      void parents1(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/56");
        
         storyText.text = @"- I received an accurate report from the parents about strangers who visited their home on the week of the murder.  ";
         SetTextPosition(storyText, 180f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, -60f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to continue";
         SetTextPosition(actionText, 140.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"";
         SetTextPosition(actionText1, -80.3f, -170f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.win_final;
         }
          
     }

      void crime_scene_final(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/57");
        
         storyText.text = @"- I've learned enough. I walked from the beach to the nearest place where I could leave the car. And from there I returned to the place where the buses from Backhill were parked. ";
         SetTextPosition(storyText, 180f, -40f);
         SetTextSize(storyText, 400f, 200f);
         
         storyText1.text =  @"";
         SetTextPosition(storyText1, -60f, -80f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"press space to continue";
         SetTextPosition(actionText, 140.3f, -140f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"";
         SetTextPosition(actionText1, -80.3f, -170f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);

          if (Input.GetKeyDown(KeyCode.Space)){
              state = StoryState.win_final;
         }
          
     }

      void win_final(){
        
         backgroundVideo.source = VideoSource.VideoClip;
         backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/58");
        
         storyText.text = @"Let's start with the fact that I realized that our criminal has a systemic way of thinking. All his crimes fit into a certain alphabetical progression. 
On the other hand, he chose his victims in a rather strange way: Mrs. Asher, Betty Bernard, Sir Clark - they were all very different from each other. 
If a person kills without any system, then usually he does it in order to remove everyone who stands in his way or annoys him. 
But the presence of an alphabetical progression told us that this was not our case. Choosing A.B.C allowed me to think about a person, as I called it for myself, with a railway mindset. 
This is more common for men than women. It was absolutely clear that the presence of a man selling stockings at or near the crime scene could not have been mere coincidence, and this indicated that the seller was the killer...  ";
         SetTextPosition(storyText, -80f, 40f);
         SetTextSize(storyText, 530f, 200f);
        
         storyText1.text =  @"- The seller turned out to be the brother of the deceased Sir Clark. Thank you for helping me solve this difficult case! ";
         SetTextPosition(storyText1, -90f, -140f);
         SetTextSize(storyText1, 530f, 100f);

         actionText.text = @"CONGRATULATIONS! YOU WIN!";
         SetTextPosition(actionText, 100.3f, -240f);
         SetTextSize(actionText, 530f, 100f);
         actionText1.text = @"";
         SetTextPosition(actionText1, -80.3f, -170f);
         SetTextSize(actionText1, 530f, 100f);
         actionText2.text = @"";
         SetTextPosition(actionText2, -100.3f, -180f);
         SetTextSize(actionText2, 530f, 100f);
          
     }

}







